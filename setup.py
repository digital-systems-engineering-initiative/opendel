from setuptools import setup, find_packages

setup(
   name='opendel',
   version='0.0.1',
   description='Open Digital Engineering Library',
   author='Joe Verbist',
   author_email='joe@joeverbist.com',
   packages=find_packages(include=['OpenDEL']),  # would be the same as name
   install_requires=[
       "pandas",
       "pint",
       "numpy",
       "plotly"
   ]
)
