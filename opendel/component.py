from typing import List
from .metric import Metric


class Component:
    def __init__(self):
        self.children: List['Component'] = []
        self.metrics: List['Metric'] = []
        self.operators = None  # Functional operators
        self.parent: 'Component'

    def register_children(self):
        for child in self.children:
            child.parent = self
