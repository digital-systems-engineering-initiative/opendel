from .metric import Metric, global_metrics, register_metrics, unit_registry
from .component import Component
from .config import Config

from typing import List


class OpenDEL:
    """
    Handles all stateful behaviour of the OpenDEL library.

    """
    def __init__(self, base_object, object_config, metrics: List[Metric]):
        self.global_metrics = global_metrics
        self.unit_reg = unit_registry
        register_metrics(metrics)


        self.base_object = base_object(config=object_config)


    def aggregate_metric(self, metric_name: str):
        return self.base_object.weight()
