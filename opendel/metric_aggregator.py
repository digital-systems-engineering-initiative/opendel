from OpenDEL.component import Component
from OpenDEL.metric import _metrics
from OpenDEL.errors import MetricNotFoundError
from typing import List


def sum(obj_lst: List[Component], metric_name: str):
    # First we find the requested metric:
    metric = None
    for m in _metrics:
        if m.name == metric_name:
            metric = m

    if metric is None:
        raise MetricNotFoundError(metric_name)

    # Now that we have the metric we can go down the list of metrics
    value = 0
    for child in obj_lst:
        value += child.metric
