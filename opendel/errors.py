

class MetricNotFoundError(Exception):
    def __init__(self, metric_name: str):
        super().__init__(f"Metric with name '{metric_name}' not found")
