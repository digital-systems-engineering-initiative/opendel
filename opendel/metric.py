from pint import Quantity, Unit, UnitRegistry
from typing import List
from functools import wraps
from copy import deepcopy


global_metrics = []
unit_registry = UnitRegistry(system='mks')


class Metric:
    def __init__(self, name: str, unit: Unit):
        self.name = name
        self._value = None
        self._unit = unit

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value: Quantity):
        assert value.dimensionality == self.unit.dimensionality
        self._value = value

    @property
    def unit(self):
        return self._unit

    def __add__(self, other):
        assert self.name == other.name
        assert self.unit.dimensionality == other.unit.dimensionality
        m = Metric(name=self.name, unit=self._unit)
        m.value = self.value + other.value
        return m

    def __repr__(self):
        return f"<Weight {self.value}>"


def register_metrics(metric_lst: List[Metric]):
    # We need to check that the names are unique:
    names = [metric.name for metric in metric_lst]
    assert len(set(names)) == len(names)

    # Then we can add them to the list.
    for metric in metric_lst:
        global_metrics.append(metric)


def component_metric(name: str):
    # First we check if the metric has actually been registered
    m = None

    def outer_decorator(func):
        @wraps(func)
        def inner_decorator(*args, **kwargs):
            for metric in global_metrics:
                if metric.name == name:
                    m = metric

            if m is None:
                raise Exception(f"Metric with name {name} not found")

            # As we always wrap functions that are in classes
            # The first element of *args is the class' self.
            # hence:
            host_class = args[0]
            host_class.metrics.append(m)

            kwargs['metric'] = deepcopy(m)

            return func(*args, **kwargs)
        return inner_decorator

    return outer_decorator
